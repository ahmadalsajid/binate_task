import sys
import os
import requests
from PIL import Image
from io import BytesIO


def get_image_urls(file_name):
    """
    This method takes a file name as argument and tries to read
    image urls from the file and return them as e list.
    """
    try:
        with open(file_name) as file:
            # get all image urls from images.txt file
            lines = file.readlines()
            # remove all '\n' from urls
            urls = [line.rstrip() for line in lines]
            # return the urls in a list
            return urls
    except Exception as e:
        # some error occurred, printing the error in terminal for debugging
        print(str(e), 'Failed to get image urls from file.')
        return None


def download_images(parent_dir, urls):
    """
    This method takes a directory path as first argument and a list of urls
    as second argument. It then tries to get image from each url and save
    image in the targeted directory
    """
    target_dir = os.path.join(parent_dir, 'my_images')
    for url in urls:
        try:
            # get image object from url
            image_object = requests.get(url)
            # get image name from splitting the url
            image_name = url.split("/")[-1]
            # get the image from image_object
            image = Image.open(BytesIO(image_object.content))
            # save the image in the targeted folder
            image.save(os.path.join(target_dir, image_name + "." + image.format), image.format)
        except Exception as e:
            # error in downloading file
            print(e)
            print('failed url: ', url)


if __name__ == '__main__':
    try:
        # try to get the file name from command line that is passed as argument.
        # if no file name is passed, it will take 'images.txt' as default file
        # name that is located in the same directory.
        _file = sys.argv[1] if len(sys.argv) > 1 else 'images.txt'
        # get all urls from the file using method "get_image_urls()"
        urls = get_image_urls(_file)
        # if there is any error in getting the urls, raise error
        if not urls:
            raise Exception('Could not process file to get image urls')
        # lets assume we are saving the files in a folder named
        # "my_images". if the directory does not exist, we will
        # create it in the same path where this script is kept.
        working_dir = os.path.dirname(os.path.realpath(sys.argv[0]))
        if not os.path.exists(os.path.join(working_dir, 'my_images')):
            os.makedirs(os.path.join(working_dir, 'my_images'))
        # now, download the images from urls using "download_images()" method.
        download_images(working_dir, urls)

    except Exception as e:
        print(str(e))
