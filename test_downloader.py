from downloader import get_image_urls
import unittest


class TestDownloader(unittest.TestCase):
    def test_get_image_urls_without_file_name(self):
        self.assertRaises(TypeError, get_image_urls)

    def test_get_image_urls_with_file_name(self):
        result = get_image_urls('images.txt')
        self.assertIsInstance(result, list)

    def test_get_image_urls_with_wrong_file_name(self):
        self.assertRaises(FileNotFoundError, get_image_urls('test.txt'))


if __name__ == '__main__':
    unittest.main()
