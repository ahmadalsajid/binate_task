# BINATE TASK

#### Virtual environment setup
This task is developed and tested in Ubuntu 16.04 machine. So it is preferred you use Linux environment to test the codes.
You will need `python 3.6` to run the scripts. It is preferred to use `virtualenv` to create and test the scripts. Create virtual environment with `python 3.6` in your machine using the command
```
virtualenv -p python3.6 your_envname
```
Activate virtualenv
```
. path/to/virtualenv/your_envname/bin/activate
```
Install dependencies from requirements.txt
```
pip install -r requirements.txt
```

#### Usage
Now open terminal and `cd` to the directory where `downloader.py` is located. You have to place the text file containing image urls in the same directory. Now, to run the script, use the command
```
python downloader.py images.txt
```
Here, `images.txt` is the file name as it is passed to the script as command line argument.When the script is executed, it will try to create a subdirectory named `my_images` if it is not already created and store the images collected from the urls in the text file.

To run the unittest, provide the command in terminal
```
python -m unittest
```


